<?php
/**
 * Plugin name: robots.txt for Multisite
 * Description: Генерирует robots.txt для мультисайта
 * Author: AK
 * Version:     0.1.2
 */

/**
 * Генерирует robots.txt для мультисайта
 *
 * @param $output
 * @param $public
 *
 * @return string
 */
function ak_custom_robots( $output, $public ) {
	$site_full_url =  site_url();
	$output = "User-agent: *\n";
	$output .= "Crawl-delay: 10\n";
	$output .= "Allow: /wp-content/uploads/\n";
	$output .= "Disallow: /searcher/\n";
	$output .= "Disallow: /wp-login.php\n";
	$output .= "Disallow: /wp-register.php\n";
	$output .= "Disallow: /cgi-bin/\n";
	$output .= "Disallow: /wp-admin/\n";
	$output .= "Disallow: /trackback\n";
	$output .= "Disallow: */trackback\n";
	$output .= "Disallow: */*/trackback\n";
	$output .= "Disallow: */*/feed\n";
	$output .= "Disallow: */feed\n";
	$output .= "Disallow: /*?*\n";
	$output .= "Disallow: /comment-page-*\n";
	$output .= "Disallow: /comments\n";
	$output .= "Disallow: /*/?replytocom=*\n";
	$output .= "Disallow: /category/\n";
	$output .= "Disallow: /author/\n";
	$output .= "Disallow: /page/*\n";
	$output .= "Allow: /*/*.css\n";
	$output .= "Allow: /*/*.js\n";
	$output .= "\n";
	$output .= "User-agent: Yandex\n";
	$output .= "Allow: /wp-content/uploads/\n";
	$output .= "Disallow: /searcher/\n";
	$output .= "Disallow: /wp-login.php\n";
	$output .= "Disallow: /wp-register.php\n";
	$output .= "Disallow: /cgi-bin/\n";
	$output .= "Disallow: /wp-admin/\n";
	$output .= "Disallow: /trackback\n";
	$output .= "Disallow: */trackback\n";
	$output .= "Disallow: */*/trackback\n";
	$output .= "Disallow: */*/feed\n";
	$output .= "Disallow: /*?*\n";
	$output .= "Disallow: */feed\n";
	$output .= "Disallow: /comment-page-*\n";
	$output .= "Disallow: /comments\n";
	$output .= "Disallow: /*/?replytocom=*\n";
	$output .= "Disallow: /category/\n";
	$output .= "Disallow: /author/\n";
	$output .= "Disallow: /js/\n";
	$output .= "Disallow: /page/*\n";
	$output .= "Allow: /*/*.css\n";
	$output .= "Allow: /*/*.js\n";
	$output .= "\n";
	//if(in_array('all-in-one-seo-pack/all_in_one_seo_pack.php', apply_filters('active_plugins', get_option('active_plugins')))){
	if ( is_plugin_active( 'all-in-one-seo-pack/all_in_one_seo_pack.php' ) ) {
		$output .= "Sitemap: $site_full_url/sitemap.xml.gz\n";
	} else {
		$output .= "Sitemap: $site_full_url/sitemap.xml\n";
	}

	return $output;
}

add_filter( 'robots_txt', 'ak_custom_robots', 20, 2 );
